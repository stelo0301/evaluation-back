<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class eleves extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('eleves')->insert([
            'name' => Str::random(10),
            'surname' => Str::random(10),
            'adress' => Str::random(10),
            'date of birth'=> Str::random(10),
            'date of inscription'=> Str::random(10),
        ]);
    }
}